# Void Linux aarch64

This installation guide assumes you are comfortable using the CLI. The instructions provided are to install Void Linux aarch64 in a VM using software such as Parallels Desktop, UTM, or any similar software on an ARM machine.

In my case, I have Void Linux virtualized using both Parallels and UTM on my MacBook Pro M1 machine.

As always, you assume any risk from blindly copy-pasting code into your machine. I am **<u>NOT</u>** responsible nor am I liable to any damages you might incur.


## Getting started

I have used Arch Linux aarch64 to ROOTFS install Void Linux, you can use any distribution you are comfortable with. Just note that you would need to replace the `pacman <operation> [options] [targets]` with its equivalent in the distribution you are using.

You can grab a copy to live boot Arch Linux from [here](https://archboot.com/iso/aarch64/latest/).

Additionally, you would want a copy of Void Linux aarch64. You can grab a copy from [here](https://voidlinux.org/download/#arm). I have used the glibc version, I don't see why the musl version would be any different in terms of the installation procedure outlined here.

## Preparing the drive

Start by live booting Arch Linux and getting to the CLI. Once there, issue the following commands to get some tools that are needed to perform the installation.

Sync pacman:
```
pacman -Sy
```

Install `wget` and `dosfstools`:
```
pacman -Su wget dosfstools
```

Use `cfdisk` or any tool you are comfortable with to partition the drive. Make sure that the partition table is `gpt`. You will need to create 2 partitions (3 if you want a swap partition). This guide will assume you want swap and the instructions given take that into account.

|   Device  |       Size        |       Type         |
| --------- | :---------------: | :----------------: |
| /dev/vda1 |       512M        |   EFI System       |
| /dev/vda2 |       2.0G        |   Linux swap       |
| /dev/vda3 |   Remaining space |   Linux filesystem |

### ⚠️ NOTE:
#### Your partition labels might be `/dev/sdXY` or `/dev/nvmeXnYpZ` instead of `/dev/vdXY`. **BE CAUTIOUS!**

Now prepare the partitions as follows:
```
mkfs.vfat /dev/vda1     # Format BOOT partition
mkswap /dev/vda2        # Format SWAP partition
mkfs.ext4 /dev/vda3     # Format ROOT partition
```

Lastly, we mount them:
```
# Mount  ROOT
mount /dev/vda3 /mnt/

# Create mountpoint for BOOT
mkdir -p /mnt/boot/efi/

# Mount  BOOT
mount /dev/vda1 /mnt/boot/efi/

# Enable SWAP
swapon /dev/vda2
```

## CHROOT

Obtain a copy of the ROOTFS tarball image and extract it:

```
# Change the link to point to the most up-to-date release
wget https://repo-default.voidlinux.org/live/current/void-aarch64-ROOTFS-20230628.tar.xz

# Extract to /mnt
tar -xvf void-aarch64-ROOTFS-20230628.tar.xz -C /mnt
```

Manually chroot by mounting the following directories:

```
# Copy for internet access
cp /etc/resolv.conf /mnt/etc
cp /etc/hosts /mnt/etc

# Mount Kernel Virtual File Systems
mount --rbind /sys /mnt/sys && mount --make-rslave /mnt/sys
mount --rbind /dev /mnt/dev && mount --make-rslave /mnt/dev
mount --rbind /proc /mnt/proc && mount --make-rslave /mnt/proc

# Change to the new root
chroot /mnt /bin/bash
```

### Sidenote for the interested

**chroot** is short for _(ch)ange (root)_. In our case, we are operating inside a live system. There's a hard drive that we want to install Void Linux on. We want to be able to work on that hard drive.

In order to do this, we would like to change the root. We do so by changing the system root to be `/mnt` on the live system. The `/mnt` directory on the live system currently has the hard drive filesystem mounted in it. By running `chroot /mnt /bin/bash`, the new root is now `/mnt`.

In other words, **chroot** changes what is perceived as your root (i.e. `/`) to be something else (i.e. `/mnt`).

## Installing Void Linux

Sync and update `xbps`, install base-system + kernel + other packages of interest.

```
xbps-install -Suy xbps
xbps-install -uy
xbps-install -uy base-system linux nano elogind polkit dbus chrony git wget curl NetworkManager
xbps-remove -y base-voidstrap
```

Set your timezone by creating a symbolic link in `/etc/localtime`. For example, if you are in New York (or EST in general), you can set your timezone like so:

```
ln -sf /usr/share/zoneinfo/America/New_York /etc/localtime
```

In addition, you can generate your locales by uncommenting the desired locales in `/etc/default/libc-locales` and then running the following

```
xbps-reconfigure -f glibc-locales
```

It is advisable to change the _**root**_ user password as so

```
# Change root user password
passwd
```

You will not be able to see what you are typing, but realize that the terminal is still recording your input.


You can create a user (recommended) as follows. In this example, the HOSTNAME is _**VoidDomain**_, USERNAME is _**void**_ and belongs to someone NAMED _**VoidUser**_. 

```
# Add domain
echo "VoidDomain" > /etc/hostname
echo "127.0.0.1    localhost
::1          localhost
127.0.1.1    VoidDomain.localdomain VoidDomain" >> /etc/hosts

# Add user and set password
useradd void -m -c "VoidUser" -s /bin/bash
passwd void

# Add user to wheel group
usermod -aG wheel,audio,video,optical,storage void

# Configure sudo for 'wheel' group
visudo # Uncomment %wheel ALL=(ALL:ALL) ALL
```

You will need to edit `/etc/fstab`. You can read more about `fstab` [here](https://www.redhat.com/sysadmin/etc-fstab) if you are interested. In short, `fstab`, or file system table, contains a list of volumes to be mounted at boot time.

```
# /dev/vda3: /
UUID=abcdefgh-0123-4567-8910-ijklmnopqrst / ext4 rw,relatime 0 1

# /dev/vda1: /boot/efi
UUID=1234-ABCD /boot/efi vfat rw,relatime,fmask=0022,dmask=0022,codepage=437,iocharset=ascii,shortname=mixed,errors=remount-ro 0 2

# /dev/vda2: swap
UUID=abcdefgh-0123-4567-8910-ijklmnopqrst	swap 	swap 	rw,noatime,discard 	0 0

# tmpfs
tmpfs /tmp tmpfs defaults,nosuid,nodev 0 0
```

where the `UUID` for each partition can be found using `blkid`.

Now install and configure grub,

```
xbps-install -uy grub-arm64-efi
grub-install --target=arm64-efi --efi-directory=/boot/efi --bootloader-id="Void"
```

If you get an error stating the EFI variables can't be found, execute the following command and try `grub-install` again.

```
mount -t efivarfs none /sys/firmware/efi/efivars
```

Lastly, run the following to make sure everything is configured properly.

```
update-grub
xbps-reconfigure -fa
```

Exit the chroot environment and unmount everything.

```
exit
umount -a
```

You might get warnings such as device is busy, that's not ideal, but is still okay. I personally kept getting those warnings, and I am not sure why. If you have an idea on how to fix those warnings, please let me know.

Now reboot and enjoy,

```
shutdown -r now
```

## Troubleshooting

I had an issue where after I would restart and boot the VM, everything would run fine, but I would never get a login prompt or any sign of an active shell to use. However, booting in recovery mode still worked and allowed me to use a shell to log in and apply some fixes.

The culprit, in my case at least, was the fact that agetty, short for _(a)lternative (ge)t (tty)_, was not being invoked at run time. To fix this, link the service to runit. From [Void Linux docs](https://docs.voidlinux.org/config/services/index.html), the service can be linked directly into the default `runsvdir`:

```
ln -s /etc/sv/<service> /etc/runit/runsvdir/default/
```

This will automatically start the service. Once a service is linked it will always start on boot and restart if it stops, unless administratively downed.

Therefore, 

```
ln -s /etc/sv/agetty-console /etc/runit/runsvdir/default/
```

will have agetty-console running at boot.


## Miscellaneous

Enable RTC (real-time clock) service

```
ln -s /etc/sv/chronyd /var/service/
```

Enable network-related services

```
ln -s /etc/sv/{dhcpcd,NetworkManager} /var/service/
```

Enable services for seat (tldr; multiple users can use the same pc as if it was their own)

```
ln -srf /etc/sv/{dbus,polkitd,elogind} /var/service
```

You might want to do the following for networking access on boot

```
echo "ip link set eth0 up
dhcpcd" >> /etc/rc.local
```

# Automated Script

I will provide a script to automate the process whenever I get the chance. It is mainly for personal use, but feel free to use it yourself.

I have the write-up of it ready but need to package it up nicely and run through it a handful of times to make sure it works as intended.

## Instructions

### Part 1
Once you boot into live Arch Linux. You will need to sync pacman before you can grab git.

```
pacman -Sy
pacman -S git
```

Get a copy of the script by cloning this repository

```
git clone https://gitlab.com/mohammadodeh/void-linux-aarch64.git
```

Run the script

```
cd ./void-linux-aarch64
chmod +x prepare_void.sh
sudo ./prepare_void.sh --install
```

After following the instructions, you will be in the chroot'ed directory. You may proceed to [Part 2](#part-2).

### Part 2
W.I.P