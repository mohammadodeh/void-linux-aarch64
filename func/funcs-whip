#!/bin/bash
# shellcheck disable=SC2034,SC2154
{
	# //////////////////////////////////
	# Global helper functions
	#
	# //////////////////////////////////
	# Author : Mohammad Odeh
	# Contact: mohammadodeh.com
	#
	# //////////////////////////////////

	unset -v G_PROGRAM_NAME
	
	# G_WHIP_INPUTBOX "message"
	# - Prompt user to input text and save it to G_WHIP_RETURNED_VALUE
	# - Exit code: 0=input done, else=user cancelled or noninteractive
	# ==========================================================================
	# WHIP_INPUTBOX "$WHIP_TITLE" "$WHIP_BACKTITLE" "$WHIP_PROMPT"
	# --------------------------------------------------------------------------
	# - "$WHIP_TITLE"					: Variable containing box title
	# - "$WHIP_BACKTITLE"				: Variable containing back title
	# - "$WHIP_PROMPT"					: Variable containing menu prompt
	# ==========================================================================
	WHIP_INPUTBOX()
	{
		# Unpack inputs
		local TITLE=$1					# Save first  argument in a variable
		local BACKTITLE=$2				# Save second argument in a variable
		local PROMPT=$3					# Save third  argument in a variable

		local result=1
		local WHIP_ERROR=''

		unset -v RETURN_VAL # in case left from last G_WHIP

		while :
		do

			RETURN_VAL=$("$dialog" \
					--title "$TITLE" \
					--backtitle "$BACKTITLE" \
					--inputbox "$WHIP_ERROR$PROMPT" \
					--ok-button "Enter" \
					--cancel-button "Cancel" 15 60 3>&1 1>&2 2>&3-)
					
			result=$?
			[[ $result == 0 && -z $RETURN_VAL ]] &&
			{
				WHIP_ERROR='[FAILED] An input value was not entered, please try again...\n\n';
				continue;
			}
			break
		done
		
		echo "$RETURN_VAL"
		return "$result"
	}

	# ==========================================================================
	# WHIP_MENU "$WHIP_TITLE" "$WHIP_PROMPT" "${WHIP_MENU[@]}"
	# --------------------------------------------------------------------------
	# - "$WHIP_TITLE"					: Variable containing box title
	# - "$WHIP_PROMPT"					: Variable containing menu prompt
	# - "${WHIP_MENU[@]}"				: Array containing menu items
	# ==========================================================================
	WHIP_MENU()
	{
		# Unpack inputs
		local TITLE=$1					# Save first  argument in a variable
		local PROMPT=$2					# Save second argument in a variable
		shift; shift      				# Shift all arguments to the left (original $1, $2 gets lost)
		local WHIP_MENU=("$@") 			# Rebuild the array with rest of arguments

		local MENU=() 					# Initialize empty array
		local CNTR=0 					# Loop counter
		for entry in "${WHIP_MENU[@]}"; do
			MENU+=("$entry")
		done

		# //////////////////////////////
		# Note to self:
		# 	Math in bash is don as follows
		#	-> x=3
		#	-> y=2
		#	-> z="$((x+y))" # leads to z=5
		#
		local list_len="$((${#WHIP_MENU[@]}/2))"	# Length of list
		
		"$dialog" \
			--title "$TITLE" \
			--menu  "$PROMPT" 15 60 "$list_len" \
					"${MENU[@]}" 3>&1 1>&2 2>&3
	}


	# ==========================================================================
	# WHIP_MSG "$WHIP_TITLE" "$WHIP_PROMPT"
	# --------------------------------------------------------------------------
	# - "$WHIP_TITLE"					: Variable containing box title
	# - "$WHIP_PROMPT"					: Variable containing menu prompt
	# ==========================================================================
	WHIP_MSG()
	{
		# Unpack inputs
		local TITLE=$1					# Save first  argument in a variable
		local PROMPT=$2					# Save second argument in a variable

		"$dialog" \
			--title "$TITLE" \
			--msgbox "$PROMPT" 15 60 5
	}


	# ==========================================================================
	# WHIP_YESNO "$WHIP_TITLE" "$WHIP_PROMPT"
	# --------------------------------------------------------------------------
	# - "$WHIP_TITLE"					: Variable containing box title
	# - "$WHIP_PROMPT"					: Variable containing menu prompt
	# ==========================================================================
	WHIP_YESNO()
	{
		# Unpack inputs
		local TITLE=$1					# Save first  argument in a variable
		local PROMPT=$2					# Save second argument in a variable

		"$dialog" \
			--title "$TITLE" \
			--yesno "$PROMPT" 15 60 4 \
			--defaultno \
			--yes-button "Proceed" \
			--no-button "Exit"
	}

	# check whether whiptail or dialog is installed
	# (choosing the first command found)
	read -r dialog <<< "$(which whiptail dialog 2> /dev/null)"

	# exit if none found
	[[ "$dialog" ]] || {
		echo 'neither whiptail nor dialog found' >&2
		exit 1
	}

	# just use whichever was found
	"$dialog" --msgbox "Message displayed with $dialog" 0 0

}
